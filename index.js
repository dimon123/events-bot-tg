const TelegramBot = require('node-telegram-bot-api');
const { Client } = require('pg');
const { Sequelize, DataTypes } = require('sequelize');


const { dbClient, sequelize } = require('./dbConfig');
const bot = require('./telegramConfig');


  const Event = sequelize.define('Event', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    userid: { type: DataTypes.INTEGER, allowNull: false },
    authorName: { type: DataTypes.STRING, allowNull: false },
    data: { type: DataTypes.STRING, allowNull: false },
    time: { type: DataTypes.STRING, allowNull: false },
    title: { type: DataTypes.STRING, allowNull: false },
    description: { type: DataTypes.STRING, allowNull: false },
    country: { type: DataTypes.STRING, allowNull: false },
    city: { type: DataTypes.STRING, allowNull: false },
    addres: { type: DataTypes.STRING, allowNull: false },
    contactsdetails: { type: DataTypes.STRING, allowNull: true },
    active: { type: DataTypes.BOOLEAN, defaultValue: true },
   // categoryId : { type: DataTypes.INTEGER, allowNull: false },
  });

  const User = sequelize.define('User', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    userid: { type: DataTypes.INTEGER, allowNull: false },
    name: { type: DataTypes.STRING, allowNull: false },
    nickname: { type: DataTypes.STRING},
  });

  const Category = sequelize.define('Category', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    NameOfCategory: { type: DataTypes.STRING, allowNull: false },
  });

  const Country = sequelize.define('Country', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    CountryName: { type: DataTypes.STRING, allowNull: false },
  });

  const City = sequelize.define('City', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    CityName: { type: DataTypes.STRING, allowNull: false },
  });

  
  // Створення таблиці "events" у базі даних, якщо вона не існує
  User.sync()
  .then(() => {
    console.log('Таблиця "users" була створена або вже існує.');
  })

  .catch((err) => {
    console.error('Помилка під час створення таблиці "users":', err);
  });

  Event.sync()
  .then(() => {
    console.log('Таблиця "events" була створена або вже існує.');
  })
  .catch((err) => {
    console.error('Помилка під час створення таблиці "events":', err);
  });

  
  City.sync()
  .then(() => {
    console.log('Таблиця "City" була створена або вже існує.');
  })
  .catch((err) => {
    console.error('Помилка під час створення таблиці "City":', err);
  });

  
  Country.sync()
  .then(() => {
    console.log('Таблиця "Country" була створена або вже існує.');
  })
  .catch((err) => {
    console.error('Помилка під час створення таблиці "Country":', err);
  });

  
  Category.sync()
  .then(() => {
    console.log('Таблиця "Category" була створена або вже існує.');
  })
  .catch((err) => {
    console.error('Помилка під час створення таблиці "Category":', err);
  });


  const eventInfo = {}; // Зберігання інформації про івент
  const userState = {};
// Обробник події старту бота
bot.onText(/\/start/, async (msg) => {
    const chatId = msg.chat.id;
    const userid = msg.from.id;
    const nickname = msg.from.username;
    const name = msg.from.first_name;
    

    // Створюємо клавіатуру з двома кнопками
    const existingUser = await User.findOne({ where: { userid } });


try {
  





  if (!existingUser) {
    // Якщо користувач ще не існує, створіть нового користувача в базі даних
    await User.create({ userid, name, nickname });
}
} catch (err) {
  console.error(err);
      bot.sendMessage(userid, 'При додаванні користувача виникла помилка');
}
    

    const keyboard = {
        reply_markup: {
            keyboard: [['Усі івенти'], ['Мої івенти'], ['Додати власний івент'] ],
        },
    };

    bot.sendMessage(chatId, 'Виберіть опцію:', keyboard);
});


bot.onText(/Мої івенти/, async (msg) => {
 

  const chatId = msg.chat.id;
  const userId = msg.from.id;

  const keyboard = {
    reply_markup: {
        keyboard: [['Видалити івент'], ['Назад'] ],
    },
};

bot.sendMessage(chatId,  'Виберіть опцію:', keyboard);


  try {
      // Знайдіть всі івенти, які належать конкретному користувачеві
      const userEvents = await Event.findAll({ where: { userid: userId } });

      if (userEvents.length > 0) {
          const eventList = userEvents
              .map((event) => {
                  return `Назва: ${event.title}\nОпис: ${event.description}\nДата: ${event.data}\nЧас: ${event.time}\nКраїна: ${event.country}\nМісто: ${event.city}\nКонтакти: ${event.contactsdetails}\n`;
              })
              .join('\n\n');
          bot.sendMessage(chatId, 'Список ваших івентів:\n\n' + eventList);
      } else {
          bot.sendMessage(chatId, 'На жаль, ви не створили жодних івентів.');
      }
  } catch (err) {
      console.error(err);
      bot.sendMessage(chatId, 'Під час отримання ваших івентів виникла помилка.');
  }
});

bot.onText(/Назад/, (msg) => {
  const chatId = msg.chat.id; // Отримуємо chatId

  // Створюємо клавіатуру з основними кнопками
  const keyboard = {
      reply_markup: {
          keyboard: [['Усі івенти'], ['Мої івенти'], ['Додати власний івент']],
      },
  };

  bot.sendMessage(chatId, 'Повернутися до головного меню:', keyboard);
});
bot.onText(/Видалити івент/, async (msg) => {
  const chatId = msg.chat.id;
  const userId = msg.from.id;

  // Встановіть стан користувача, який очікує вибору івента для видалення
  userState[userId] = 'chooseEventToDelete';

  try {
    // Отримайте всі івенти, які належать користувачеві
    const userEvents = await Event.findAll({ where: { userid: userId } });

    if (userEvents.length > 0) {
      // Створіть клавіатуру з кнопками, де кожна кнопка відповідає івенту користувача
      const eventButtons = userEvents.map((event) => [event.title]);
      // Додайте кнопку "Назад" до меню
      eventButtons.push(['Назад']);

      const keyboard = {
        reply_markup: {
          keyboard: eventButtons,
          one_time_keyboard: true,
        },
      };

      bot.sendMessage(chatId, 'Оберіть івент, який ви хочете видалити:', keyboard);
    } else {
      bot.sendMessage(chatId, 'На жаль, ви не створили жодних івентів для видалення.');
      // Скиньте стан користувача
      delete userState[userId];
    }
  } catch (err) {
    console.error(err);
    bot.sendMessage(chatId, 'Під час отримання ваших івентів виникла помилка.');
    // Скиньте стан користувача
    delete userState[userId];
  }
});


// Обробник вибору івента для видалення
bot.on('text', async (msg) => {
  const chatId = msg.chat.id;
  const userId = msg.from.id;
  const userText = msg.text;

  if (userState[userId] === 'chooseEventToDelete') {
    if (userText === 'Назад') {
      
      delete userState[userId];
    } else {
      // Отримайте інформацію про івент, який користувач хоче видалити за назвою
      const eventToDelete = await Event.findOne({
        where: {
          userid: userId,
          title: userText,
        },
      });

      if (eventToDelete) {
        // Видаліть івент
        await eventToDelete.destroy();
        const keyboard = {
          reply_markup: {
              keyboard: [['Усі івенти'], ['Мої івенти'], ['Додати власний івент']],
          },
      };

        bot.sendMessage(chatId, 'Івент був успішно видалений.', keyboard);
        
      } else {
        bot.sendMessage(chatId, 'Івент з такою назвою не був знайдений.');
      }

      // Скиньте стан користувача
      delete userState[userId];

      // Після видалення повторно відобразіть кнопки "/start"
     
    }
  }
});


bot.onText(/Усі івенти/, async (msg) => {
    const chatId = msg.chat.id;

    
  


//{ where: { categoryId } }

    try {
      const events = await Event.findAll();
  
      if (events.length > 0) {
        const eventList = events
          .map((event) => {
            return ` 🔥  ${event.title}🔥 \n ${event.description}\n ${event.data}\n ${event.time} \n ${event.city}\n${event.contactsdetails}\n  by ${event.authorName} \n\n`;
          })
          .join('\n');
        bot.sendMessage(chatId, 'Список всіх івентів: \n \n' + eventList);
      } else {
        bot.sendMessage(chatId, 'На жаль, жодних івентів не знайдено. Але ви завжди можете додати власний!)');
      }
    } catch (err) {
      console.error(err);
      bot.sendMessage(chatId, 'Під час отримання івентів виникла помилка.');
    }
  });
 
  
  

   // Словник для відстеження поточного стану користувача

  

  bot.onText(/Додати власний івент/, (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
   
  



  const userUsername = msg.from.username;
  if (userUsername !== 'DmitriuN') {
    bot.sendMessage(chatId, 'Ви не маєте дозволу на додавання івентів. Зверніться до адміністратора');
    return;
  }

  const keyboard = {
    reply_markup: {
      keyboard: [['Назад']],
    },
  };


  
  
  bot.sendMessage(chatId,  'Заповніть поля нижче', keyboard);

  
  if (userState[userId] === 'chooseEventToDelete') {
    if (userText === 'Назад') {
      delete userState[userId];
      const keyboard = {
        reply_markup: {
          keyboard: [['Усі івенти'], ['Мої івенти'], ['Додати власний івент']],
        },
      };
      bot.sendMessage(chatId, 'Повернутися до головного меню:', keyboard);
    }
  }


    // Встановлюємо, що ми очікуємо назву івенту
    userState[userId] = 'title';
  
    bot.sendMessage(chatId, 'Введіть назву івенту:');
  });
  
  // Обробник всіх текстових повідомлень
  bot.on('text', async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const userText = msg.text;
   const authorName = msg.from.first_name;
  
    if (userState[userId]) {
      const field = userState[userId];
  
      if (field === 'title') {
        
        eventInfo.title = userText;
        userState[userId] = 'description';
        bot.sendMessage(chatId, 'Введіть опис івенту:');

      } else if (field === 'description') {
        eventInfo.description = userText;
        userState[userId] = 'contactInfo';
        bot.sendMessage(chatId, 'Введіть контакти організатора:');
        
      } else if (field === 'contactInfo') {
        eventInfo.contactsdetails = userText;
        userState[userId] = 'date';
        bot.sendMessage(chatId, 'Введіть дату івенту (наприклад, 2023-12-31):');
      }
      else if (field === 'date') {
        eventInfo.date = userText;
        userState[userId] = 'time';
        bot.sendMessage(chatId, 'Введіть час коли відбудеться івент:');
      }else if (field === 'time') {
        eventInfo.time = userText;
        userState[userId] = 'addres';
        bot.sendMessage(chatId, 'Введіть адресу за якою відбудеться подія:');

      }else if (field === 'addres') {
        eventInfo.addres = userText;
        userState[userId] = 'country';
        bot.sendMessage(chatId, 'Введіть країну івенту:');

      }
       else if (field === 'country') {
        eventInfo.country = userText;
        userState[userId] = 'city';
        bot.sendMessage(chatId, 'Введіть місто івенту:');
      } else if (field === 'city') {
        eventInfo.city = userText;
        userState[userId] = 'done';
  
        // Після отримання всіх полів, збережіть івент в базі даних
        await saveEvent(chatId, userId, eventInfo, authorName );
        delete userState[userId]; // Скидаємо стан користувача
      }
    }
  });
 
  // Функція для збереження івенту в базі даних
  async function saveEvent(chatId, userId, eventInfo, authorName) {
    try {
      const newEvent = await Event.create({
        userid: userId,
        
        title: eventInfo.title,
        description: eventInfo.description,
        data: eventInfo.date,
        time: eventInfo.time,
        country: eventInfo.country,
        city: eventInfo.city,
        contactsdetails:eventInfo.contactsdetails,
        addres:eventInfo.addres,
        authorName:  authorName
        
      });
      
  
      const eventMessage = `Івент був успішно створений!\n\n` +
      `Назва: ${eventInfo.title}\n` +
      `Опис: ${eventInfo.description}\n` +
      `Дата: ${eventInfo.date}\n` +
      `Час: ${eventInfo.time}\n` +
      `Країна: ${eventInfo.country}\n` +
      `Місто: ${eventInfo.city}\n` +
      `Контакти: ${eventInfo.contactsdetails}\n`;

      const keyboard = {
        reply_markup: {
            keyboard: [['Усі івенти'], ['Мої івенти'], ['Додати власний івент']],
        },
    };
    bot.sendMessage(chatId, eventMessage, keyboard);

     try {
            const allUsers = await User.findAll({ where: { nickname: 'kovolexiy'} });//
            const newEventMessage = `Привіт!)  Додано новий цікавий івент: ${eventInfo.title}\nДата: ${eventInfo.date}\nЧас: ${eventInfo.time}\nМісце: ${eventInfo.city}`;
            allUsers.forEach((user) => {
              if (user.userid !== userId) {
                bot.sendMessage(user.userid, newEventMessage);
              }
            });
        }catch (err) 
        {
          console.error(err);
          bot.sendMessage(chatId, 'Не вдалося відправити деяким юзерам повідомлення про новий івент');
        }


    } catch (err) {
      console.error(err);
      bot.sendMessage(chatId, 'Під час створення івенту виникла помилка.');
    }
  
  }


